import socket
import threading
from serverSocket import ServerSocket
from client import Client
from packet import Packet
import time
from broadcast import Broadcast
import logging.config
from electionClient import Election
from myToken import Token

class Balise(threading.Thread):
    otherBalise = None
    broadcast = None
    server = None
    gotToken = False
    

    def __init__(self):
        super().__init__()
        self.ip = 0
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('client')
        self.otherBalise = dict()
        self.token = None
        self.electionMode = False
        self.electionTimer = None
        self.nbResponse = 0
        self.startElectionTimer()

    def updateSelfIp(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        self.ip = s.getsockname()[0]
        s.close()

    def resetElectionTimer(self):
        self.electionTimer.cancel()
        self.startElectionTimer()

    def startElectionTimer(self):
        self.logger.info("start timer")
        self.electionTimer = threading.Timer(12, self.setElectionMode)
        self.electionTimer.start()

    def createToken(self):
        self.gotToken = True
        self.token = Token()
        self.token.baliseIps.append(self.ip)

    def setElectionMode(self):
        if not self.electionMode:
            self.gotToken = False
            self.electionMode = True
            election = Election(Election.START, self)
            election.start()


    def printBalises(self):
        #self.logger.info(self.otherBalise)
        threading.Timer(4, self.printBalises).start()

    #Ajoute l'ip à la liste de la balise si inexistante et si non = a localhost
    def addIp(self, ip):
        if ip != self.ip:
            if ip not in self.otherBalise.keys():
                self.otherBalise[ip] = 0
            self.sendPacket(ip, Packet("ack hello","ACK_HELLO"))
                

    #Ajoute 1 aux ip de la balise à chaque cycle Hello
    def incrementBaliseAddress(self):
        for ip in self.otherBalise.keys():
            self.otherBalise[ip] = self.otherBalise[ip] + 1

    def resetIncrementBalise(self, ip):
        if ip in self.otherBalise.keys():
            self.otherBalise[ip] = 0

    #Supprimer une ip si elle n'a pas répondu depuis 4 cycle Hello
    def checkIp(self):
        keys = list(self.otherBalise.items())
        for ip in keys:
            if ip[1] >= 4:
                del self.otherBalise[ip[0]]       
        threading.Timer(5, self.checkIp).start()

    def sendPacket(self, ip, packet):
        client = Client(ip, packet, self)
        client.start()

    def run(self):
        self.updateSelfIp()
        self.printBalises()
        self.checkIp()
        server = ServerSocket(self)
        broadcast = Broadcast(self)
        server.start()
        broadcast.start()
        while True:
            if self.gotToken and not self.electionMode:
                gotOne = False
                if len(self.otherBalise) > 0:
                    #self.logger.info('more than 0')
                    for ip, compt in self.otherBalise.items():
                        if gotOne:
                            break
                        if not ip in self.token.baliseIps:
                            if compt == 0:
                                # send token to this ip
                                # create a packet
                                packet = Packet(self.token, "TOKEN")
                                self.sendPacket(ip, packet)
                                self.logger.info("TOKEN SENT")
                                self.gotToken = False
                                gotOne = True
                                pass
                    if not gotOne:
                        # reset token baliseIps
                        #self.logger.info('reset token baliseIps')
                        self.token.baliseIps = []
                        self.token.baliseIps.append(self.ip)
            #self.logger.info("try")
            time.sleep(1)

        

balise = Balise()
balise.start()

