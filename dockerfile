FROM python:3
COPY balise.py /
COPY broadcast.py /
COPY client.py /
COPY packet.py /
COPY server.py /
COPY serverSocket.py /
COPY logging.conf /
COPY myToken.py /
COPY electionClient.py /
CMD [ "python3", "./balise.py" ]
