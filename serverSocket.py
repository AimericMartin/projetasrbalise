import threading
import logging.config
import socket
from server import Server

class ServerSocket(threading.Thread):

    port = 65434

    def __init__(self, balise):
        super().__init__()
        self.balise = balise
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('server')


    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.balise.ip, self.port))
            while True:
                s.listen()
                conn, addr = s.accept()
                server = Server(conn, addr, self.balise)
                server.start()

    