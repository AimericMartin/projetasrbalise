
import threading
import logging.config
import socket
import json

from packet import Packet


class Client(threading.Thread):

    def __init__(self, ip, packet, balise):
        super().__init__()
        self.ip = ip
        self.packet = packet
        self.port = 65435
        self.balise = balise
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('client')

    
    def run(self):
        #self.logger.info("Client started to send " + str(self.packet.type) + " to " + str(self.ip))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.ip, 65434))

        s.send(json.dumps(self.packet.serialize()).encode(encoding='utf-8'))
        
        try:
            s.settimeout(5)
            response = s.recv(1024)
            decoded = json.loads(response.decode(encoding='utf-8'))
            if(decoded['type']== Packet.ACK):
                #self.logger.info("ack reçu, fermeture client")
                s.close()
                return
            if (decoded['type'] == Packet.ACK_ELECTION):
                # self.logger.info("ack reçu, fermeture client")
                self.balise.nbResponse += 1
                s.close()
                return
        except:
            pass
        #self.logger.info(decoded)
        
        