
import threading
import logging.config
import socket
import json
import time
from client import Client
from packet import Packet

class Election(threading.Thread):
    RUN = "RUN"
    START = "START"

    def __init__(self, mode, balise):
        super().__init__()
        self.balise = balise
        self.mode = mode
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('election')

    def run(self):
        if self.mode == Election.RUN:
            self.running()
        else:
            self.starting()

    def starting(self):
        self.logger.info("starting")
        packet = Packet('', Packet.START_ELECTION)
        for ip, compt in self.balise.otherBalise.items():
            Client(ip, packet, self.balise).start()
        self.running()

    def gotOneRunning(self, clients):
        for client in clients:
            if client.is_alive():
                return True
            else:
                clients.remove(client)
            return False

    def running(self):
        self.logger.info("running")
        clients = []
        packet = Packet('', Packet.RUNNING_ELECTION)
        for ip, compt in self.balise.otherBalise.items():
            if ip > self.balise.ip:
                client = Client(ip, packet, self.balise)
                clients.append(client)
                client.start()
        while self.gotOneRunning(clients):
            time.sleep(1)
        if self.balise.nbResponse == 0:
            self.elected()
        self.balise.nbResponse = 0


    def elected(self):
        self.logger.info("elected")
        packet = Packet('', Packet.ELECTED)
        for ip, compt in self.balise.otherBalise.items():
            Client(ip, packet, self.balise).start()
        time.sleep(2)
        self.balise.electionMode = False
        self.balise.createToken()
        self.balise.resetElectionTimer()



