
from myToken import Token


class Packet:
    START_ELECTION = "START_ELECTION"
    RUNNING_ELECTION = "RUNNING_ELECTION"
    ELECTED = "ELECTED"
    ACK = "ACK"
    ACK_ELECTION = "ACK_ELECTION"

    def __init__(self, payload, typePacket):
        self.payload = payload
        self.type = typePacket

    def serialize(self):

        if isinstance(self.payload, Token):
            return {
                "payload": self.payload.serialize(),
                "type": self.type
            }
        return {
            "payload": self.payload,
            "type": self.type
        }