import threading 
import logging.config
import socket

class Broadcast(threading.Thread):

    port = 65433
    otherBalise = None

    def __init__(self,balise):
        super().__init__()
        self.balise = balise
        self.otherBalise = self.balise.otherBalise
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('client')

    def sendHello(self):
        #self.logger.info("Sending Hello to everyone")
        dest = ('<broadcast>', 65433)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.sendto(self.balise.ip.encode(), dest)
        s.close()
        self.balise.incrementBaliseAddress()
        threading.Timer(4,self.sendHello).start()

    def run(self):
        #self.logger.info("Starting the broadcast routine")
        self.sendHello()
        while 1:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            s.bind(('', 65433))
            message = s.recv(1024)
            self.balise.addIp(message.decode())
            #self.logger.info("Received " + message.decode() + " from " + addr[0])
            