from electionClient import Election
import threading
import logging.config
import socket
import time
import json
from packet import Packet
from myToken import Token


class Server(threading.Thread):

    def __init__(self, s, addr, balise):
        super().__init__()
        logging.config.fileConfig("logging.conf")
        self.logger = logging.getLogger('serverTh')
        self.socket = s
        self.speakingWith = addr
        self.balise = balise

    def run(self):
        # self.logger.info('server speaking with:' + str(self.speakingWith))
        self.logger.info("new connection")
        with self.socket:
            try:
                data = self.socket.recv(1024)
            except ConnectionError:
                self.socket.close()
                return
            try:
                decoded = json.loads(data.decode(encoding='utf-8'))
            except:
                pass
            # Si on repond à notre hello on reinitialise le compteur car la balise existe encore
            self.logger.info(decoded['type'])
            if decoded['type'] == 'ACK_HELLO':
                self.balise.resetIncrementBalise(self.speakingWith[0])

                # self.logger.info(self.speakingWith)

            if decoded['type'] == 'TOKEN' and self.balise.gotToken == False:
                self.logger.info("TOKEN RECEIVED")
                #self.logger.info(decoded['payload'])
                self.balise.token = Token(decoded['payload']['detections'], decoded['payload']['baliseIps'])
                self.balise.token.baliseIps.append(self.balise.ip)
                self.balise.gotToken = True
                self.balise.resetElectionTimer()

            if decoded['type'] == Packet.START_ELECTION:
                if not self.balise.electionMode:
                    self.balise.electionMode = True
                    self.balise.gotToken = False
                    election = Election(Election.RUN, self.balise)
                    election.start()

            if decoded['type'] == Packet.ELECTED:
                self.logger.info("got packet elected")
                self.balise.electionMode = False
                self.balise.resetElectionTimer()

            if decoded['type'] == Packet.RUNNING_ELECTION:
                packetStr = Packet("packet reçu", Packet.ACK_ELECTION).serialize()
            else:
                # self.logger.info("received data : " + str(decoded))
                packetStr = Packet("packet reçu", Packet.ACK).serialize()
            try:
                self.socket.send(json.dumps(packetStr).encode(encoding='utf-8'))
            except ConnectionError:
                self.socket.close()
                return
            time.sleep(1)
