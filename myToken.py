class Token:
    detections = {}
    baliseIps = []

    def __init__(self, detections=None, baliseIps=None):
        if baliseIps is None:
            baliseIps = []
        if detections is None:
            detections = {}
        self.detections = detections
        self.baliseIps = baliseIps

    def serialize(self):
        return {
            'detections': self.detections,
            'baliseIps': self.baliseIps
        }